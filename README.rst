BWA Index 0.7.17 GeneFlow App
=============================

Version: 0.1

This GeneFlow app wraps the BWA 0.7.17 index tool.

Inputs
------

1. reference: Reference Sequence FASTA - A reference sequence in FASTA file format to be indexed for BWA. The default value for this input is "/reference/sequence.fa", which is an invalid path. Thus, this input must be provided when running a workflow with this app. 

Parameters
----------

1. output: Output Directory - The directory at which the new BWA index will be generated. The default value for the output directory is "reference". This directory is relative to the workflow or step output location. 
